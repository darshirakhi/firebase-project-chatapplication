package com.example.rakhi.firebasemessaging;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Signup extends AppCompatActivity {

    private EditText email,password;
    private Button register;
    private ProgressBar progressbar;

    private FirebaseAuth firebase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        email=(EditText)findViewById(R.id.signup_email);
        password=(EditText)findViewById(R.id.signup_password);
        register=(Button)findViewById(R.id.register_button);
        progressbar=(ProgressBar)findViewById(R.id.signup_progressBar);

        firebase=FirebaseAuth.getInstance();



        register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String Email=email.getText().toString().trim();
                final String Password=password.getText().toString().trim();

                if(TextUtils.isEmpty(Email)){
                    Toast.makeText(Signup.this, "Email required", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.isEmpty(Password)){
                    Toast.makeText(Signup.this, "Password Required", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(Password.length()<6){
                    Toast.makeText(Signup.this, "Password should be atleast 6 characters", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressbar.setVisibility(View.VISIBLE);

                firebase.createUserWithEmailAndPassword(Email,Password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressbar.setVisibility(View.GONE);
                        if(!task.isSuccessful()){

                            Toast.makeText(Signup.this,task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                        }else{
                            Toast.makeText(Signup.this, "Registration Successful", Toast.LENGTH_SHORT).show();

                            Intent i=new Intent(Signup.this,Login.class);
                            startActivity(i);
                            finish();

                        }
                    }
                });
            }
        });
    }
}
