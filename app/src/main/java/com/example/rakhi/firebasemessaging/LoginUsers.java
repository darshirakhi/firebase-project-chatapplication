package com.example.rakhi.firebasemessaging;

/**
 * Created by Rakhi on 08-07-2017.
 */
public class LoginUsers {
    private String first_name="";
    private String last_name="";
    private String age="";
    private String email="";
    private String username="";


    public LoginUsers() {
    }


    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }


}


