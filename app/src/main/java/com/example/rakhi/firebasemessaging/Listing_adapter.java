package com.example.rakhi.firebasemessaging;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Rakhi on 01-07-2017.
 */
public class Listing_adapter extends BaseAdapter {
    //con,layoutinflater and users for initialising constructor
    Context con;
    LayoutInflater layoutInflater;
    ArrayList<Users> users;

    public Listing_adapter(Context context, ArrayList<Users> users){
        con=context;
        layoutInflater=LayoutInflater.from(con);
        this.users=users;
    }
    @Override
    public int getCount() {

        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public class ViewHolder{

        TextView username,email;
        ImageView user_image;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        //we use convertView instead of findviewById for fast response.Calling findViewById can make your listview being populated by
        //data slow. the screen that is visible to you ,for each of the listview 'row' -a new convertview is created
        //and data from the viewholder is populated into the convertview.Once all the view(that is listview rowvisible to you on phone's screen without scrolling ) is populated by data
        //when you scroll up ,a row that goes up that row is recycled to get used to be populated by data for a below row-the
        //row that became visible after scrolling
        if(convertView == null){
            convertView=layoutInflater.inflate(R.layout.list_row,null,false);
            viewHolder=new ViewHolder();
            viewHolder.user_image=(ImageView)convertView.findViewById(R.id.user_image);
            viewHolder.username=(TextView)convertView.findViewById(R.id.user_name);
            viewHolder.email=(TextView)convertView.findViewById(R.id.user_email);
            convertView.setTag(viewHolder);//when there are many convertviews


        }else{//we use getTag once when all convertview used
            viewHolder=(ViewHolder)convertView.getTag();//Android takes your convertView and puts it in its pool to recycle it and passes it again to you.(when screen scrolled down)
            // but its pool may not have enough convertViews so it again passes a new convertView thats null. so again the story is
            // repeated till the pool of android is filled up. after that android takes a convertView from its pool and passes it to you.
            // you will find that its not null so you ask it where are my object references that I gave to you for the first time? (getTag)
            // so you will get those and do whatever you like.
        }
        Users user=users.get(position);
        viewHolder.username.setText(user.getUsername());
        viewHolder.user_image.setImageResource(R.drawable.rounded_rect_bg);///////////////////////////////
        viewHolder.email.setText(user.getEmail());

        return convertView;
    }


}
