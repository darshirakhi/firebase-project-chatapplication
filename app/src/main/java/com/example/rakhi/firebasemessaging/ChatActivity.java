package com.example.rakhi.firebasemessaging;

import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Rakhi on 08-07-2017.
 */


public class ChatActivity extends AppCompatActivity {
    private EditText msgEditText;
    private ImageButton send;
    private FirebaseAuth firebase;
    private DatabaseReference mdatabase;
    private String username;
    private Messageadapter adapter;
    private ListView chat_list;
    private ArrayList<Messages> messages=new ArrayList<>();
    private Boolean result;
    private TextView user_chat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        msgEditText=(EditText)findViewById(R.id.messageEditText);
        send=(ImageButton)findViewById(R.id.sendMessageButton);
        chat_list=(ListView)findViewById(R.id.chat_list);
        user_chat=(TextView)findViewById(R.id.user_chat);
        user_chat.setText(UserDetails.chatwith);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        username =  sharedPreferences.getString(getString(R.string.username), "DefaultValue");
        Log.i("username",username);

        adapter=new Messageadapter(ChatActivity.this,messages);
        chat_list.setAdapter(adapter);
        chat_list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);


        firebase=FirebaseAuth.getInstance();
        //    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();

        mdatabase= FirebaseDatabase.getInstance().getReference();
        final String chat_room1=username + "-" + UserDetails.chatwith;
        Log.i("chatroom",chat_room1);
        final String chat_room2=UserDetails.chatwith+"-"+username;


       /* mdatabase.child("messages").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {//will be called if data changed here in oncreate dat is not getting changed so not working
                if(dataSnapshot.exists()) {
                    Log.i("datasnap", String.valueOf(dataSnapshot));
                    for (DataSnapshot childrenkey : dataSnapshot.getChildren()) {
                        Log.i("Datasnapshotmessaging", String.valueOf(dataSnapshot.getChildren()));
                        String key = childrenkey.getKey();
                        Log.i("key", key);
                        if (key.equals(chat_room1)) {
                            real_chatroom[0] = chat_room1;///////////////////////////
                            Log.i("realchatroom", real_chatroom[0]);
                            break;

                        } else {
                            real_chatroom[0] = chat_room2;
                            Log.i("realchatroomelse", real_chatroom[0]);
                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
/*
        Log.i("above onclick",real_chatroom[0]);
*/
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message=msgEditText.getText().toString().trim();

                if (!message.equals("")) {
                    Map<String, String> map = new HashMap<String, String>();
                    map.put("message", message);
                    map.put("username",username);


                    mdatabase.child("messages").child(chat_room1).push().setValue(map, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if (databaseError != null) {
                                System.out.println("message could not be saved " + databaseError.getMessage());
                            } else {

                                System.out.println("message saved successfully.");
                            }
                        }
                    });
                    mdatabase.child("messages").child(chat_room2).push().setValue(map, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if (databaseError != null) {
                                System.out.println("message could not be saved " + databaseError.getMessage());
                            } else {

                                System.out.println("message saved successfully.");
                            }
                        }
                    });

                }
            }
        });//child event listener will always accompany after pushing data because it is on child added,as soon as child gets added itt should show
        //send button onclick k bahar hoga retrieve data otherwise multiple data will show in listview
        //coz takes some time to add and retrieve


/*
        Log.i("retrieve",real_chatroom[0]);
*/
        mdatabase.child("messages").child(chat_room2).addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Messages msg=dataSnapshot.getValue(Messages.class);
                String retreive_username=msg.getUsername();
                if(retreive_username.equals(username)){
                    result=true;
                }else{
                    result=false;
                }
                Log.i("messages",msg.getMessage());
                messages.add(msg);
                adapter.notifyDataSetChanged();//getview method of messageadapter is called and the observer of the listview refreshes all the data and redraws the listrow
                adapter.registerDataSetObserver(new DataSetObserver() {
                    @Override
                    public void onChanged() {
                        super.onChanged();
                        chat_list.setSelection(adapter.getCount()-1);
                    }
                });

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

}
