package com.example.rakhi.firebasemessaging;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Register extends AppCompatActivity{
        private EditText email,password,fname,lname,age,username;
        private Button register;
        private DatabaseReference mdatabase;
        private FirebaseAuth firebase;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_profile);

            fname=(EditText)findViewById(R.id.fname);
            lname=(EditText)findViewById(R.id.lname);
            age=(EditText)findViewById(R.id.age);
            username=(EditText)findViewById(R.id.reg_username);
            email=(EditText)findViewById(R.id.reg_email);
            password=(EditText)findViewById(R.id.reg_password);
            register=(Button)findViewById(R.id.register);

            firebase=FirebaseAuth.getInstance();
            mdatabase= FirebaseDatabase.getInstance().getReference().child("users");


            register.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    final String Fname=fname.getText().toString().trim();
                    final String Lname=lname.getText().toString().trim();
                    final String Age=age.getText().toString().trim();
                    final String Username=username.getText().toString().trim();
                    final String Email=email.getText().toString().trim();
                    final String Password=password.getText().toString().trim();


                    if(ValidateForm()){

                        firebase.createUserWithEmailAndPassword(Email,Password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                Users user=new Users();
                                user.setFirst_name(Fname);
                                user.setLast_name(Lname);
                                user.setAge(Age);
                                user.setEmail(Email);
                                user.setUsername(Username);

                                //if task not successful
                                if(!task.isSuccessful()){
                                   // Log.d("Loginactivity","Registration successful:"+task.isSuccessful());

                                    Toast.makeText(Register.this,task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                //if task successful,note for each insertion of data in databse add- addOnCompleteListener
                                }else{
                                    Toast.makeText(Register.this, "Registration Successful", Toast.LENGTH_SHORT).show();

                                    //if reg successful get uid of user and push data of user under uid in database
                                    mdatabase.child(firebase.getCurrentUser().getUid()).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            Toast.makeText(Register.this, "user registered to database", Toast.LENGTH_SHORT).show();
                                            Intent i=new Intent(Register.this,Login.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    });

                                }
                            }
                        });


/*
                        mdatabase.child("users").push().setValue(user);
*/
                    }
                }
            });
        }


    public boolean ValidateForm(){
        boolean alldone=true;
        String Fname=fname.getText().toString().trim();
        String Lname=lname.getText().toString().trim();
        String Age=age.getText().toString().trim();
        String Email=email.getText().toString().trim();
        String Password=password.getText().toString().trim();
        String Username=username.getText().toString().trim();

        if(TextUtils.isEmpty(Fname)){
            fname.setError("Enter your first name");
            alldone=false;
        }else{
            alldone=true;
            fname.setError(null);
        }

        if(TextUtils.isEmpty(Lname)){
            lname.setError("Enter your last name");
            alldone=false;
        }else{
            alldone=true;
            lname.setError(null);
        }

        if(TextUtils.isEmpty(Age)){
            alldone=false;
            age.setError("Enter your age");
        }else{
            alldone=true;
            age.setError(null);
        }


        if(TextUtils.isEmpty(Email)){
            alldone=false;
            email.setError("Enter your email");
        }else{
            alldone=true;
            age.setError(null);
        }


        if(TextUtils.isEmpty(Password)){
            alldone=false;
            password.setError("Enter your password");
        }else{
            alldone=true;
            password.setError(null);
        }


        if(Password.length()<6){
            alldone=false;
            password.setError("Password should be atleast 6 characters");
        }else{
            alldone=true;
            password.setError(null);
        }


        if(!isValidEmailAddress(Email)){
            alldone=false;
            email.setError("Enter a valid email address");
        }else{
            alldone=true;
            email.setError(null);
        }

        if(TextUtils.isEmpty(Username)){
            username.setError("Enter your user name");
            alldone=false;
        }else{
            alldone=true;
            username.setError(null);
        }

        return alldone;


    }


    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}


