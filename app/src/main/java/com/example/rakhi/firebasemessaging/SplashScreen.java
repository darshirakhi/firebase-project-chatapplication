package com.example.rakhi.firebasemessaging;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Rakhi on 05-07-2017.
 */
public class SplashScreen extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        Thread background=new Thread(){
            @Override
            public void run() {
                //sleep thread for 5 seconds
                try {
                    sleep(5*1000);

                    //after 5 seconds start activity
                    Intent i=new Intent(SplashScreen.this,Login.class);
                    startActivity(i);

                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        background.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
