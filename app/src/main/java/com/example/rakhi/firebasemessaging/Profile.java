package com.example.rakhi.firebasemessaging;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import droidninja.filepicker.models.Media;

public class Profile extends AppCompatActivity{

    private TextView profile_name,profile_username,profile_email,profile_age;
    private SharedPreferences sharedPreferences;
    private String FirstName,LastName,Name;
    FloatingActionButton pic;
    private static final int MY_PERMISSIONS_REQUEST = 123;
    private static final int PICK_IMAGE_REQUEST_CODE = 213;
    private Uri outputFileUri;
    ArrayList <String> filepaths;
    ImageView imageview;



    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("images");
    StorageReference  storageReference = FirebaseStorage.getInstance().getReference();//firebase storage
    private DatabaseReference previousref=FirebaseDatabase.getInstance().getReference().getRoot().child("previous prescription");



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_edit_display);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)//gallery permission
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        android.Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST);
        }
        profile_name=(TextView)findViewById(R.id.user_profile_name);
        profile_username=(TextView)findViewById(R.id.profile_username);
        profile_email=(TextView)findViewById(R.id.profile_email);
        profile_age=(TextView)findViewById(R.id.profile_age);
        pic=(FloatingActionButton)findViewById(R.id.user_change_pic);

        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        FirstName=sharedPreferences.getString("first name","no value");
        LastName=sharedPreferences.getString("last name","no value");
        Name=FirstName+" "+LastName;
        profile_name.setText(Name);
        profile_username.setText(sharedPreferences.getString("username","no value"));
        profile_email.setText(sharedPreferences.getString("email","no value"));
        profile_age.setText(sharedPreferences.getString("age","no value"));
        filepaths = new ArrayList<>();
        imageview=(ImageView)findViewById(R.id.header_cover_image);
        pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filepaths.clear();

                FilePickerBuilder.getInstance().setMaxCount(1).setSelectedFiles(filepaths).setActivityTheme(R.style.AppTheme).pickPhoto(Profile.this);


            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if(requestCode == FilePickerConst.REQUEST_CODE_PHOTO && data!=null){
                filepaths = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA);

                for(String path : filepaths){
                    final Uri uri = Uri.fromFile(new File(path));

                    Log.i("imagepath",String.valueOf(uri));
                    Log.i("imagepath",String.valueOf(uri));
                    Log.i("imagepath",String.valueOf(uri));
                    Log.i("imagepath",String.valueOf(uri));
                    Log.i("imagepath",String.valueOf(uri));
                    Log.i("imagepath",String.valueOf(uri));
                    Log.i("imagepath",String.valueOf(uri));

                    Glide.with(Profile.this).load(uri).into(imageview);

                    StorageReference reference = storageReference.child("images").child(uri.getLastPathSegment());
                    final ProgressDialog progressDialog = new ProgressDialog(this);

                    reference.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                            progressDialog.dismiss();

                                            //and displaying a success toast
                                            Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception exception) {
                                            //if the upload is not successfull
                                            //hiding the progress dialog
                                            progressDialog.dismiss();

                                            //and displaying error message
                                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    })
                                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                            //calculating progress percentage
                                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                                            //displaying percentage in progress dialog
                                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                                        }
                                    });
                        }


                        }




                }


    }

}



