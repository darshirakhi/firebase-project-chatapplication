package com.example.rakhi.firebasemessaging;

/**
 * Created by Rakhi on 07-07-2017.
 */
public class Messages {
    private String username="";
    private String message="";

    public Messages() {
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUsername() {
        return username;
    }

    public String getMessage() {
        return message;
    }
}
