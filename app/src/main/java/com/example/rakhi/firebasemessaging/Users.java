package com.example.rakhi.firebasemessaging;

/**
 * Created by Rakhi on 01-07-2017.
 */
public class Users {
    private String first_name="";
    private String last_name="";
    private String age="";
/*
    private String profile_image="";
*/
    private String email="";
    private String username="";

    public Users(){}
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setAge(String age) {
        this.age = age;
    }

    /*public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }*/

    public void setEmail(String email) {
        this.email = email;
    }
     public void setUsername(String username){this.username=username;}

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getAge() {
        return age;
    }

    /*public String getProfile_image() {
        return profile_image;
    }*/
    public String getEmail() {
        return email;

    }

    public String getUsername(){
        return username;
    }


}
