package com.example.rakhi.firebasemessaging;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.v7.widget.CardView;
import android.text.Layout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Rakhi on 07-07-2017.
 */
public class Messageadapter extends BaseAdapter {

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<Messages> messages;
    String login_username;


    public Messageadapter(Context context,ArrayList<Messages> messages){
        this.context=context;
        this.layoutInflater=layoutInflater.from(context);
        this.messages=messages;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        login_username =  sharedPreferences.getString(context.getResources().getString(R.string.username), "DefaultValue");


    }
    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {//parent is the listview
        TextView username,message;
        CardView cardview;
        LinearLayout chat_layout;


        if(convertView==null){//checks if convertview that we received is empty/ blank

            convertView=layoutInflater.inflate(R.layout.message_row,parent,false);//then inflate something
            }

        username=(TextView)convertView.findViewById(R.id.chat_username);//and find by id
        message=(TextView)convertView.findViewById(R.id.chat_msg);
        cardview=(CardView)convertView.findViewById(R.id.card);

        chat_layout=(LinearLayout)convertView.findViewById(R.id.chat_layout);
//else if already we have received some convertview that is inflated with some view find view by id

        Messages msg=messages.get(position);
        String Username=msg.getUsername();
        String Message=msg.getMessage();
        if(Username.equals(login_username)){
            chat_layout.setGravity(Gravity.LEFT);

/*
            cardview.setBackground(context.getResources().getDrawable(R.drawable.chat_bubble));
*/

        }else{
            chat_layout.setGravity(Gravity.RIGHT);
/*
            cardview.setBackground(context.getResources().getDrawable(R.drawable.chat_bubble2));
*/
        }

        username.setText(Username);
        message.setText(Message);

        return convertView;
    }
}
